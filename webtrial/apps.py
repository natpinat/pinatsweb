from django.apps import AppConfig


class WebtrialConfig(AppConfig):
    name = 'webtrial'
