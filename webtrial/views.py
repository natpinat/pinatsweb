from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,'home.html',{})

def about(request):
    return render(request,'about.html',{})

def info(request):
    return render(request,'info.html',{})

def portfolio(request):
    return render(request,'portfolio.html',{})

def artPortfolio(request):
    return render(request,'artPortfolio.html',{})

def otherPortfolio(request):
    return render(request,'otherPortfolio.html',{})

def news(request):
    return render(request,'news.html',{})
